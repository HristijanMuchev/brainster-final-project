import React from "react";
import Logo from "./brainster_space_logo.svg";
import "./Footer.css";
// import { faCoffee } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const Footer = (props) => {
  return (
    <div className="Footer">
      <div className="container-fluid footerFirstRow">
        <div className="row">
          <div class="col-md-2 col-md-offset-1 padding10">
            <p className="font-weight-bold paddingBottom10">Корисни линкови</p>
            <p
              className="cursorPointer"
              data-toggle="modal"
              data-target="#myModal"
            >
              Контакт
            </p>
            <a className="colorInherit" href="https://www.wearelaika.com/">
              <p>Отворени позиции</p>
            </a>
            <a
              className="colorInherit"
              href="https://medium.com/wearelaika/brainster-space-the-new-home-of-the-local-tech-community-in-skopje-ffe97b564152"
            >
              <p>Галерија</p>
            </a>
            <a className="colorInherit" href="/">
              <p>Календар</p>
            </a>
          </div>
          <div className="col-md-2 padding10">
            <p className="font-weight-bold paddingBandL10">Социјални мрежи</p>
            <a href="https://www.facebook.com/brainster.co" target="_blank">
              <i className="fa fa-facebook fb"></i>
            </a>
            <a
              href="https://www.linkedin.com/school/brainster-co/"
              target="_blank"
            >
              <i className="fa fa-linkedin linked"></i>
            </a>
            <a href="https://www.instagram.com/brainsterco/" target="_blank">
              <i className="fa fa-instagram instagram"></i>
            </a>
          </div>
          <div className="col-md-2 col-md-offset-5 padding10">
            <a className="navbar-brand" href="#">
              <img src={Logo} alt=""></img>
            </a>
          </div>
        </div>
      </div>
      <div className="footer">
        <p className="blockMargin">
          COPYRIGHT&copy;BrainsterSpace. All Rights Reserved
        </p>
      </div>
    </div>
  );
};
export default Footer;
