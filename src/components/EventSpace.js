import React from "react";
import Space from "./HorizontalComponent/Space";
import EventHost from "./HorizontalComponents2/EventHost";
import First from "./HorizontalComponents2/First";
import Offer from "./HorizontalComponents2/Offer";
import OurSpace from "./HorizontalComponents2/OurSpace";
import SpaceKitchen from "./HorizontalComponents2/SpaceKitchen";
// import Card from "./Cards/Card";

const EventSpace = (props) => {
  return (
    <div className="EventSpace">
      {/* <Space /> */}

      <First />

      <OurSpace />

      <SpaceKitchen />

      <Offer />

      <EventHost />
    </div>
  );
};

export default EventSpace;
