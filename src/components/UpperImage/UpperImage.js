import React from "react";
import "./UpperImage.css";

const UpperImage = (props) => {
  return (
    <div className="UpperImage">
      <div className="bg-image">
        <div className="container-floid opacity">
          <h1 className="top">
            Центар за Учење, Кариера и <br /> Иновации{" "}
          </h1>
        </div>
      </div>
    </div>
  );
};

export default UpperImage;
