import React from "react";
import "./EventHost.css";

const EventHost = (props) => {
  return (
    <div className="container" id="x">
      <div className="row verticalAlign proba">
        <div className="col-md-6 col-lg-7">
          <h1 className="font-weight-bold fontSize35em">Event Host</h1>
          <p>
            Ања Макеска
            <br />
            <br />
            anja@brainster.co
            <br />
            <br />
            +389 (0)70 233 414
          </p>
        </div>
        <div class="col-md-6 col-lg-5">
          <img
            src={require("../../img/Za_Nas/coworking.jpg")}
            className="imgeventh"
            alt="..."
          />
        </div>
      </div>
    </div>
  );
};

export default EventHost;
