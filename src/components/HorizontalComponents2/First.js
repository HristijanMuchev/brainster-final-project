import React from "react";

const First = (props) => {
  return (
    <div className="container">
      <div className="row vertical">
        <div className="col-lg-5 col-md-12">
          <h1>
            Простор за
            <br /> настани
          </h1>
          <p>
            Нашиот простор се прилагодува според потребите на вашиот настан.
            Седум различни простории и Space Kitchen.
            <br /> <br />
            Наменски создадени да се прилагодуваат и менуваат во согласност со
            типот на настан кој го организирате.
            <br /> <br />
            Организираме конференции до 150 учесници и обуки и предавања за
            групи до 20 учесници. Контактираје не за да ви оствариме одличен
            настан.
          </p>
          <div>
            <a href="#x">
              <button className="btn danger1" id="educationBtn">
                → &nbsp; ПРОСТОР ЗА НАСТАНИ
              </button>{" "}
            </a>
          </div>
        </div>

        <div className="col-lg-7 col-md-12">
          <img
            className="img-responsive"
            src={require("../../img/Za_Nas/prostor za nastani.jpg")}
          />
        </div>
      </div>
    </div>
  );
};

export default First;
