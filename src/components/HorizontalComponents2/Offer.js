import React from "react";
import "./Offer.css";

const Offer = (props) => {
  return (
    <div className="container">
      <h1>Нудиме</h1>
      <div className="flex">
        <p className="danger2">
          <i className="fa fa-globe colorfa"></i>&nbsp;Простор
        </p>
        <p className="danger2">
          <i className="fa fa-space-shuttle colorfa"></i> &nbsp;Space Kitchen
        </p>
        <p className="danger2">
          <i className="fa fa-globe colorfa"></i>
          &nbsp;Логистика
        </p>
        <p className="danger2">
          <i className="fa fa-microphone colorfa"></i> &nbsp;Техничка подршка
        </p>
        <p className="danger2">
          <i className="fa fa-volume-up colorfa"></i>
          &nbsp;Звук
        </p>
        <p className="danger2">
          <i className="fa fa-lightbulb-o colorfa"></i>
          &nbsp;Светло
        </p>
        <p className="danger2">
          <i className="fa fa-sitemap colorfa"></i>
          &nbsp;Помош при Организација
        </p>
        <p className="danger2">
          <i class="fa fa-film colorfa"></i> &nbsp;Видео и Фотографија
        </p>
        <p className="danger2">
          <i class="fa fa-facebook colorfa"></i>
          &nbsp;Промоција на Социјални Мрежи
        </p>
      </div>
    </div>
  );
};

export default Offer;
