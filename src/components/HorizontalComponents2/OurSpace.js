import React from "react";
import "./OurSpace.css";

const OurSpace = (props) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-3">
          <h1>Нашите простории</h1>
          <br />
          <p>
            Комплетно адаптибилни. Една сала за 150 учесници, или 3 помали сали
            за групи од по 50 учесника. Училници за од 20 до 45 учесника.Избор
            од две локации.
            <br /> <br />
            Пулт за прием. И најважно место за network-Brainster Kitchen.
            <br /> <br />
            Како го замислувате вашиот следен настан?
          </p>
        </div>

        <div className="col-lg-9">
          <div className="row">
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/2.jpg")}
                className="img-cards"
              />
              <p>Brainster</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C3 1.jpg")}
                className="img-cards"
              />
              <p>Конференциска сала</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C2 1.jpg")}
                className="img-cards"
              />
              <p>Сала со бина</p>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C3 1.jpg")}
                className="img-cards"
              />
              <p>Адаптибилна училница</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C2 1.jpg")}
                className="img-cards"
              />
              <p>Училница</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C1 1.jpg")}
                className="img-cards"
              />
              <p>Училница</p>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/HOL KON SEDENJE.jpg")}
                className="img-cards"
              />
              <p>Хол</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C1 4.jpg")}
                className="img-cards"
              />
              <p>Канцелариски простр</p>
            </div>
            <div className="col-lg-4">
              <img
                src={require("../../img/Renderi/C1 4.jpg")}
                className="img-cards"
              />
              <p>Space Kitchen</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurSpace;
