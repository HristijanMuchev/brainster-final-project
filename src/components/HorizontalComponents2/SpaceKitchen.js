import React from "react";
import "./SpaceKitchen.css";

const SpaceKitchen = (props) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-lg-8">
          <div className="row">
            <div className="col-lg-6">
              <div className="row">
                <div className="col-lg-12">
                  <img
                    src={require("../../img/Space_Kitchen_Galerija/IMG_7777.jpg")}
                    className="img-left "
                  />
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6">
                  <img
                    src={require("../../img/Space_Kitchen_Galerija/IMG_7385.jpg")}
                    className="img-bottom-left img-responsive"
                  />
                </div>
                <div className="col-lg-6">
                  <img
                    src={require("../../img/Space_Kitchen_Galerija/IMG_7361.jpg")}
                    className="img-bottom-right img-responsive"
                  />
                </div>
              </div>
            </div>

            <div className="col-lg-6">
              <img
                src={require("../../img/Space_Kitchen_Galerija/IMG_7362.jpg")}
                className=" img-right"
              />
            </div>
          </div>
        </div>

        <div className="col-lg-4">
          <h1>Space Kitchen</h1>
          <p>
            Место каде сите настани започнуваат и завршуваат. Место каде нашиот
            тим готви и експериментира. Нашата кујна има само едно правило.
            <br /> <br />
            Храната мора да биде вегетаријанска. Пијалок, кафе или мезе. Вие
            одберете вид на кетеринг ние ќе го обезбедиме.
          </p>
        </div>
      </div>
    </div>
  );
};

export default SpaceKitchen;
