import React from "react";
import { Row } from "react-bootstrap";
import Card from "./Cards/Card";
import UpperImage from "./UpperImage/UpperImage";
import "./Academy.css";
import Education from "./HorizontalComponent/Education";
import Events from "./HorizontalComponent/Events";
import Coworking from "./HorizontalComponent/Coworking";
import { specialChars } from "@testing-library/user-event";
import Space from "./HorizontalComponent/Space";
import Partners from "./HorizontalComponent/Partners";

const Academy = (props) => {
  return (
    <div className="Academy">
      <UpperImage />

      <h1 className="forUs">За нас</h1>

      <div className="container">
        <div className="row">
          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/Za_Nas/edukacija1.jpg")}
              title="Едукација"
              body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
              href="https://www.brainster.co"
            />
          </div>

          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/Za_Nas/nastani.jpg")}
              title="Настани"
              body="Специјално курураби и организирани настани кои ги поврзуваат правите таленти со иновативните компании.
                    Идејата е да нашата tech заедница расте, се инспирира и креира преку овие настани."
              href="/"
            />
          </div>

          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/Za_Nas/coworking.jpg")}
              title="Coworking"
              body="Биди дел од tech заедницата на иноватори, креативци и претприемачи. Резервирај стол во нашата shared
                    office. Пичирај го твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да ги дели своите
                    канцеларии."
            />
          </div>
        </div>

        <div className="row">
          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/Za_Nas/prostor za nastani.jpg")}
              title="Простор за настани"
              body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
              href="/"
            />
          </div>

          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/partnerstva so tech komp.jpg")}
              title="Партнерства со Tech компании"
              body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
              href="/"
            />
          </div>

          <div className="col-lg-3 col-md-6 border">
            <Card
              image={require("../img/IMG_7397.jpg")}
              title="Иновации за компании"
              body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
            />
          </div>
        </div>
      </div>

      <Education />

      <Events />

      <Coworking />

      <Space />

      <Partners />
    </div>
  );
};

export default Academy;
