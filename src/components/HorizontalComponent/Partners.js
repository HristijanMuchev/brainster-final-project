import React from "react";
import "./Partners.css";

const Partners = (props) => {
  return (
    <div className="Partners">
      <h1>Партнери</h1>
      <h3>Имаш идеја? Отворени сме за соработка</h3>
      <a href="/eventspace">
        <button className="btn danger1" id="educationBtn">
          → &nbsp;ВИДИ ГО ПРОСТОРОТ
        </button>{" "}
      </a>
    </div>
  );
};

export default Partners;
