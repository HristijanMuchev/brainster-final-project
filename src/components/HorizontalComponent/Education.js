import React, { Component } from "react";
import "./Education.css";

class Education extends Component {
  constructor() {
    super();
    this.state = {
      title: "Едукација",
      paragraph: `Дали си подготвен да одговориш на потребите на иднината. Вистинските курсеви, академии и семинари кои ќе ти
            овозможат кариерна трансформација во областа дигитален маркетинг, дизајн, програмирање и Data Sience.`,
      url: "Za_Nas/edukacija1",
      // switch: false,
    };
  }

  toggleImage = (type) => {
    let newState = {};

    if (type == 1) {
      newState = {
        title: "Едукација",
        paragraph: `Дали си подготвен да одговориш на потребите на иднината. Вистинските курсеви, академии и семинари кои ќе ти
                    овозможат кариерна трансформација во областа дигитален маркетинг, дизајн, програмирање и Data Sience.`,
        url: "Za_Nas/edukacija1",
      };
    } else {
      newState = {
        title: "Компании",
        paragraph: `Компаниите имаат можност да ги надоградат своите тимови, а со тоа да го подобрат перформансот на својата компанија. 

                    Дигиталната трансформација се случува, а вашите комапнии треба да бидат подготвени за да се адаптираат соодветно. Обуки, семинари, курсеви или team building?
                    
                    Во Brainster Space имаме специјално обучен тим кој е подготвен да ја насочи, адаптира и сподели својата експертиза со денешните потреби на компаниите.`,
        url: "IMG_7707",
      };
    }

    // let newState = this.state.switch ? {
    //   title: "Едукација",
    //   paragraph: `Дали си подготвен да одговориш на потребите на иднината. Вистинските курсеви, академии и семинари кои ќе ти
    //               овозможат кариерна трансформација во областа дигитален маркетинг, дизајн, програмирање и Data Sience.`,
    //   url: "Za_Nas/edukacija1",
    //   switch: false,
    // } : {
    //   title: "Компании",
    //   paragraph: `Компаниите имаат можност да ги надоградат своите тимови, а со тоа да го подобрат перформансот на својата компанија.

    //               Дигиталната трансформација се случува, а вашите комапнии треба да бидат подготвени за да се адаптираат соодветно. Обуки, семинари, курсеви или team building?

    //               Во Brainster Space имаме специјално обучен тим кој е подготвен да ја насочи, адаптира и сподели својата експертиза со денешните потреби на компаниите.`,
    //   url: "IMG_7707",
    //   switch: true,
    // };

    this.setState(() => newState);
  };

  render() {
    return (
      <div className="container">
        <div className="row vertical ">
          <div className="col-lg-5 col-md-12">
            <h1>{this.state.title}</h1>
            <p>{this.state.paragraph}</p>
            <button
              className="btn danger1"
              id="educationBtn1"
              onClick={() => this.toggleImage(1)}
            >
              АКАДЕМИИ
            </button>
            <button
              className="btn danger1 danger3"
              id="companyBtn1"
              onClick={() => this.toggleImage()}
            >
              ЗА КОМПАНИИ
            </button>
          </div>

          <div className="col-lg-7 col-md-12">
            <img
              src={require(`../../img/${this.state.url}.jpg`)}
              className="img-responsive"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Education;
