import React, { Component } from "react";
import "./Coworking.css";

class Coworking extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
    };
  }

  showMessage = () => {
    this.setState((state) => (state.message = "Местата се распродадени !"));
    console.log(this.state.message);
  };

  render() {
    return (
      <div className="container">
        <div className="row vertical">
          <div className="col-lg-7 col-md-12">
            <img
              src={require("../../img/Za_Nas/coworking.jpg")}
              className="img-responsive"
            />
          </div>

          <div className="col-lg-5 col-md-12">
            <h1 className="TextDecoration">Coworking</h1>
            <p className="TextDecoration">
              Биди дел од tech заедницата на иноватори, креативци и
              претприемачи. Резервирај стол во нашата share offcie. Пичирај го
              твојот бизнис и нашиот тим заедно ќе одлучи секој месец со кого да
              ги дели своите канцеларии.
            </p>
            <div>
              <button
                className="danger1 TextDecoration"
                onClick={() => this.showMessage()}
              >
                Резервирај место
              </button>
              <br />
              <span className="msg">{this.state.message}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Coworking;
