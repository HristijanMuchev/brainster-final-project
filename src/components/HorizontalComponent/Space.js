import React from "react";

const Space = (props) => {
  return (
    <div className="container">
      <div className="row vertical">
        <div className="col-lg-5 col-md-12">
          <h1>
            Простор за
            <br /> настани
          </h1>
          <p>
            Можност за презентации, обуки, конференции, networking настани.
            Одбери ја просторијата која најмногу ќе одговара на твојата идеја.
            Го задржуваме правото да одбереме кој настан ќе се организира во
            нашиот Brainster Space.
          </p>
          <div>
            <a href="/eventspace">
              <button className="btn danger1" id="educationBtn">
                → &nbsp; ПРОСТОР ЗА НАСТАНИ
              </button>{" "}
            </a>
          </div>
        </div>

        <div className="col-lg-7 col-md-12">
          <img
            className="img-responsive"
            src={require("../../img/Za_Nas/prostor za nastani.jpg")}
          />
        </div>
      </div>
    </div>
  );
};

export default Space;
