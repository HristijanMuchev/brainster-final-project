import React, { Component } from "react";
import "./Events.css";
import Card from "../Cards/Card";

class Events extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div className="Events">
        <div className="forUs">
          <h1>Настани</h1>
        </div>

        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-6 border">
              <Card
                image={require("../../img/Nastani/IMG_7481.jpg")}
                title="Codeworks"
                body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                href="https://www.youtube.com/results?search_query=brainster"
              />
            </div>

            <div className="col-lg-3 col-md-6 border">
              <Card
                image={require("../../img/Nastani/instruktori.jpg")}
                title="Deep Drive into Marketing"
                body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                href="https://www.youtube.com/results?search_query=brainster"
              />
            </div>

            <div className="col-lg-3 col-md-6 border">
              <Card
                image={require("../../img/Nastani/Hristijan-Nosecka-1024x536.jpg")}
                title="Deep Drive into Data Science"
                body="Научи практични вештини за трансформација во кариерата. Нашата специјалност е да ти помоглеме да го
                    најдеш skill set кој ќе одговори на реалните потреви на пазарот на труд. Организираме курсеви, академии и
                    персонализирани обуки кои одговараат на реалните потреви на денешницата."
                href="https://www.youtube.com/results?search_query=brainster"
              />
            </div>
          </div>

          <div>
            <div className="BtnCalendar">
              <a href="/" target="">
                <button className="btn position height">
                  <i class="fa fa-calendar"></i> &nbsp; КАЛЕНДАР НА НАСТАНИ
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Events;
