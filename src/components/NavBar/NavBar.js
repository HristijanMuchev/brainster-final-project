import React from "react";
import "./NavBar.css";
import Logo from "./brainster_space_logo.svg";
import { Link } from "react-router-dom";
import Form from "../Join/Form";

const NavBar = (props) => {
  return (
    <div className="NavBar">
      <nav className="nav menu">
        <div className="container">
          <div className="nav navbar-nav navbar-header">
            <a to="/">
              <img src={Logo} alt="Logo" />
            </a>
          </div>
          <div
            className="collapse navbar-collapse"
            id="bs-example-navbar-collapse-1"
          >
            <ul className="nav navbar-nav">
              <li>
                <Link to="/">Настани</Link>
              </li>
              <li>
                <Link to="/">Co-working</Link>
              </li>
              <li>
                <Link to="/academy">Академии</Link>
              </li>
              <li>
                <Link to="/eventspace">Простор за настани</Link>
              </li>
              <li>
                <Link to="/">Партнерства</Link>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li>
                <Form />
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default NavBar;
