import React, { Component } from "react";
import "./card.css";

class Card extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div class="card">
        <img
          class="card-img-top golemina"
          src={this.props.image}
          alt="Card image cap"
        />
        <div class="card-body">
          <h5 class="card-title font-weight-bold">{this.props.title}</h5>
          <p class="card-text">{this.props.body}</p>

          <div className="icon">
            <a
              href={this.props.href}
              class="padding-bottom fa fa-arrow-circle-right text-align-right"
              target=""
            ></a>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
