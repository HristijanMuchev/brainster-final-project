import React from "react";
import "./Form.css";
const overlay = {
  position: "fixed",
  backgroundColor: "black",
  zIndex: 1000,
};
const modal = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "white",
  padding: "50px",
  zIndex: 1000,
};
export default function Modal({ open, children }) {
  if (!open) return null;
  return (
    <div style={overlay}>
      <div className=" Modal" style={modal}>
        {children}
      </div>
    </div>
  );
}
