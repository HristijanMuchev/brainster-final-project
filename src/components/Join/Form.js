import React from "react";
import Modal from "./Modal";
import "./Form.css";
const Form = () => {
  const [isOpen, setIsOpen] = React.useState(false);
  return (
    <div className="Form">
      <button className="button_wrapper" onClick={() => setIsOpen(true)}>
        + &nbsp; ПРИКЛУЧИ СЕ
      </button>
      <Modal open={isOpen} onClose={() => setIsOpen(false)}>
        <div className="Form">
          <h4 class="modal-title font-weight-bold">Приклучи се</h4>
          <button className="close" onClick={() => setIsOpen(false)}>
            X
          </button>
          <br />
          <hr />
          <label htmlFor="NameLastName">Име и Презиме (задолжително)</label>
          <br />
          <input
            type="text"
            id="NameLastName"
            placeholder="Внесете Име и Презиме"
            name="NameLastName"
            required
          ></input>
          <br />
          <br />
          <label htmlFor="NameCompany">Име на Компанија (незадолжително)</label>
          <br />
          <input
            type="text"
            id="NameCompany"
            placeholder="Внесете Име на Компанија"
            name="NameCompany"
          ></input>
          <br />
          <br />
          <label htmlFor="PhoneNumber">Телефонски Број (задолжително)</label>
          <br />
          <input
            type="text"
            id="PhoneNumber"
            placeholder="Внесете Телефонски Број"
            name="PhoneNuber"
            required
          ></input>
          <br />
          <br />
          <label htmlFor="PCorporation">
            Предлог за Соработка (задолжително)
          </label>
          <br />
          <textarea
            id="PCorporation"
            placeholder="Во 300 карактери, опишете зошто сакате да соработуваме"
          ></textarea>
          <br />
          <br />
          <hr />
          <button className="button_close" onClick={() => setIsOpen(false)}>
            ИСКЛУЧИ
          </button>
          <button onClick={() => setIsOpen(false)} className="button_join">
            {" "}
            → ИСПРАТИ ФОРМА
          </button>
        </div>
      </Modal>
    </div>
  );
};
export default Form;
