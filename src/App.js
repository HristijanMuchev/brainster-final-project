import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import EventSpace from "./components/EventSpace";
import Academy from "./components/Academy";
import NavBar from "./components/NavBar/NavBar";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <BrowserRouter>
      <NavBar />

      <Switch>
        <Route path="/Academy" component={Academy} />

        <Route path="/EventSpace" component={EventSpace} />
      </Switch>

      <Footer />
    </BrowserRouter>
  );
}

export default App;
